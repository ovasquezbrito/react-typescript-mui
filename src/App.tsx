import { BrowserRouter } from 'react-router-dom';
import { AppThemeProvider, DrawerProvider } from './shared/contexts';
import { AppRoutes } from './routes/index';
import { MenuLatera } from './shared/componentes';

export const App = () => {
  return (
    <AppThemeProvider>
      <DrawerProvider>
        <BrowserRouter>
          <MenuLatera>
            <AppRoutes />
          </MenuLatera>
        </BrowserRouter>
      </DrawerProvider>
    </AppThemeProvider>
  );
};
